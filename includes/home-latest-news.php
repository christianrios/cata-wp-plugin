<?php

	/*	Latest News
	/**************************************************
	***************************************************/

	echo '<h2>Latest News</h2>';

	$args = array( 'post_type' => 'published', 'posts_per_page' => 3 );
	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post();
		the_title();
		echo '<br/>';
	endwhile;

	wp_reset_postdata();