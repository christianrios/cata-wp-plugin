<?php 

include('dashboard/display-functions.php');

/*********************
* Customizing WordPress Dashboard for WordPress
**********************/

add_action( 'admin_head', 'dashboard_title_change' );
function dashboard_title_change(){
    if ( $GLOBALS['pagenow'] != 'index.php' ){
        return;
    }

    $GLOBALS['title'] =  __( 'Home Page Overview' ); 
}


// remove unwanted dashboard widgets for relevant users
function remove_dashboard_meta() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
}
add_action( 'admin_init', 'remove_dashboard_meta' );

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function add_dashboard_widgets() {
/*
	wp_add_dashboard_widget(
		'list_of_pages',
		'CataBalzano.com Navigation Menu',
		'list_of_pages_dashboard_widget'
	);
*/
	add_meta_box('list_of_pages', 'CataBalzano.com Navigation Menu', 'list_of_pages_dashboard_widget', 'dashboard', 'side', 'high');
	add_meta_box('sidebar_list_of_widgets', 'Homepage Sidebar', 'list_sidebar_titles_dashboard_widget', 'dashboard', 'side', 'high');

	wp_add_dashboard_widget(
		'latest_news_widget',         // Widget slug.
		'Latest News',         // Title.
		'latest_news_dashboard_widget' // Display function.
	);	

//	add_meta_box('latest_news_widget', 'Latest News', 'latest_news_dashboard_widget', 'dashboard', 'side', 'high');
	
	wp_add_dashboard_widget(
		'video_of_the_week_widget',         // Widget slug.
		'Video of the Week',         // Title.
		'video_of_the_week_widget_dashboard_widget' // Display function.
	);	
		
	wp_add_dashboard_widget(
		'current_trends_widget',        	// Widget slug.
		'Current Trends',         			// Title.
		'current_trends_dashboard_widget'	// Display function.
	);	
	
	wp_add_dashboard_widget(
		'my_work_widget',         		// Widget slug.
		'My Work',         			// Title.
		'my_work_dashboard_widget' 	// Display function.
	);	
			
	wp_add_dashboard_widget(
		'makeup_widget',        			// Widget slug.
		'Current Make Up Essentials',      // Title.
		'makeup_dashboard_widget'			// Display function.
	);	
	
	wp_add_dashboard_widget(
		'runway_feature_widget',        	// Widget slug.
		'Runway Feature',         			// Title.
		'runway_feature_dashboard_widget'	// Display function.
	);	
	
}
add_action( 'wp_dashboard_setup', 'add_dashboard_widgets' );

function latest_news_dashboard_widget() {

	$post_type = 'published';

	$args = array( 'post_type' => $post_type, 'posts_per_page' => 3 );
	$loop = new WP_Query( $args );
	
	display_latest_posts_dashboard($args, $post_type);
}

function video_of_the_week_widget_dashboard_widget() {

	$post_type = 'video';

	$args = array( 'post_type' => $post_type, 'posts_per_page' => 1 );
	$video_meta_id = '_cata_video_link';
	
	display_post_with_video($args,$post_type,$video_meta_id);

}

function my_work_dashboard_widget() {

	$post_type = 'my-work';

	$args = array( 'post_type' => $post_type, 'posts_per_page' => 1 );
	$loop = new WP_Query( $args );
	
	display_latest_posts_dashboard($args, $post_type);
}

function current_trends_dashboard_widget() {

	$post_type = 'trend';

	$args = array( 'post_type' => $post_type, 'posts_per_page' => 3 );
	$loop = new WP_Query( $args );
	
	echo'<p>Each Trend will link directly to a blog post</a>';
	
	display_latest_posts_dashboard($args, $post_type);
}

function runway_feature_dashboard_widget() {

	$post_type = 'runway';

	$args = array( 'post_type' => $post_type, 'posts_per_page' => 1 );
	$video_meta_id = '_cata_runway_link';
	
	display_post_with_video($args,$post_type,$video_meta_id);
}

function makeup_dashboard_widget() {

	$post_type = 'makeup';

	$args = array( 'post_type' => $post_type, 'posts_per_page' => 1 );
	
	display_latest_posts_dashboard($args,$post_type);
}

function list_of_pages_dashboard_widget(){	
	wp_nav_menu();
	echo '<br/><hr/>';
	
	echo '<strong>';
	echo '<a href="'.admin_url().'nav-menus.php">Edit Nav Menu</a>';
	echo '</strong>';
}

function list_sidebar_titles_dashboard_widget(){
	
	$sidebar_id = 'cata-main-sidebar';
	$sidebars_widgets = wp_get_sidebars_widgets();
	$widget_ids = $sidebars_widgets[$sidebar_id]; 
    
	foreach( $widget_ids as $id ) {
        $wdgtvar = 'widget_'._get_widget_id_base( $id );
        $idvar = _get_widget_id_base( $id );
        $instance = get_option( $wdgtvar );
        $idbs = str_replace( $idvar.'-', '', $id );
		if($instance[$idbs]['title']!=''):
        	echo '<li>'.$instance[$idbs]['title'].'</li>';
		else:
			echo '<li>(no title)</li>';
		endif;
    }
	echo '<br/><hr/>';
	
	echo '<strong>';
	echo '<a href="'.admin_url().'widgets.php">Edit Sidebar Items</a>';
	echo '</strong>';
	
}