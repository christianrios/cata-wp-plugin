<?php

add_action( 'cmb2_init', 'cata_custom_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function cata_custom_metabox() {
	// Start with an underscore to hide fields from custom fields list
	$prefix = '_cata_';
	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$cmb_translations = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_translation',
		'title'         => __( 'Spanish Translation', 'cmb2' ),
		'object_types'  => array( 'post', ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		 'show_names' => false, // Show field names on the left
		 'cmb_styles' => false, // false to disable the CMB stylesheet
		 'closed'     => true, // true to keep the metabox closed by default
	) );
    
    $cmb_translations->add_field( array(
		'name'    => __( 'Spanish', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'esp',
		'type'    => 'wysiwyg',
		'options' => array( 'textarea_rows' => 9, ),
	) );
	
	$cmb_shopping = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_shopping',
		'title'         => __( 'Shopping', 'cmb2' ),
		'object_types'  => array( 'post', ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		 'closed'     => true, // true to keep the metabox closed by default
	) );

    $shopping_group_id = $cmb_shopping->add_field( array(
        'id'          => 'shopping_repeat_group',
        'type'        => 'group',
        'description' => __( '', 'cmb' ),
        'options'     => array(
            'group_title'   => __( 'Shopping Item {#}', 'cmb' ), // since version 1.1.4, {#} gets replaced by row number
            'add_button'    => __( 'Add Another Item', 'cmb' ),
            'remove_button' => __( 'Remove Item', 'cmb' ),
            'sortable'      => true, // beta
        ),
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb_shopping->add_group_field( $shopping_group_id, array(
        'name' => 'Item Title',
        'id'   => 'title',
        'type' => 'text',
        // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    $cmb_shopping->add_group_field( $shopping_group_id, array(
        'name' => 'Entry Image',
        'id'   => 'image',
        'type' => 'file',
    ) );

    $cmb_shopping->add_group_field( $shopping_group_id, array(
        'name' => 'Shopping Link',
        'id'   => 'shopping_link',
        'type' => 'text',
    ) );
	
	$cmb_instagram = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_instagram',
		'title'         => __( 'Instragram', 'cmb2' ),
		'object_types'  => array( 'post', ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		//'cmb_styles' => false, // false to disable the CMB stylesheet
		 'closed'     => true, // true to keep the metabox closed by default
	) );
	
	$cmb_instagram->add_field( array(
		'name' => __( 'Instragram Link 1', 'cmb2' ),
		'desc' => __( 'Enter the Instagram photo ID', 'cmb2' ),
		'id'   => $prefix . 'ig1',
		'type' => 'text_small',
		// 'repeatable' => true,
	) );
	$cmb_instagram->add_field( array(
		'name' => __( 'Instragram Link 2', 'cmb2' ),
		'desc' => __( 'Enter the Instagram photo ID', 'cmb2' ),
		'id'   => $prefix . 'ig2',
		'type' => 'text_small',
		// 'repeatable' => true,
	) );
	
	$cmb_instagram->add_field( array(
		'name' => __( 'Instragram Link 3', 'cmb2' ),
		'desc' => __( 'Enter the Instagram photo ID', 'cmb2' ),
		'id'   => $prefix . 'ig3',
		'type' => 'text_small',
		// 'repeatable' => true,
	) );
	
	$cmb_published = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_pub_link',
		'title'         => __( 'Link', 'cmb2' ),
		'object_types'  => array( 'published', ), // Post type
		 'show_names' => false, // Show field names on the left
	) );
    
    $cmb_published->add_field( array(
		'name'    => __( 'Link', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'pub_link',
		'type'    => 'text',
	) );
	
	$cmb_video_otw = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_votw',
		'title'         => __( 'Link', 'cmb2' ),
		'object_types'  => array( 'video', ), // Post type
		 'show_names' => false, // Show field names on the left
	) );
	 
    $cmb_video_otw->add_field( array(
		'name'    => __( 'Link', 'cmb2' ),
		'desc'    => __( 'Paste video link', 'cmb2' ),
		'id'      => $prefix . 'video_link',
		'type'    => 'oembed',
	) );
	
	$cmb_my_work = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_my_work',
		'title'         => __( 'Alternate Featured Image (NOT Homepage)', 'cmb2' ),
		'object_types'  => array( 'my-work', ), // Post type
		 'show_names' => false, // Show field names on the left
	) );
	 
    $cmb_my_work->add_field( array(
		'name'    => __( 'Featured Image for Page', 'cmb2' ),
		'desc'    => __( 'Note: If no image is uploaded, the same featured image for the home page will be used', 'cmb2' ),
		'id'      => $prefix . 'my_work_image',
		'type'    => 'file',
	) );
	
	$cmb_trends = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_trends',
		'title'         => __( 'Trend Post Info', 'cmb2' ),
		'object_types'  => array( 'trend', ), // Post type
		 'show_names' => true, // Show field names on the left
	) );
    
	$cmb_trends->add_field( array(
		'name'    => __( 'Caption', 'cmb2' ),
	    'desc'    => '',
	    'default' => '',
	    'id'      => $prefix . 'trend_caption',
	    'type'    => 'textarea_small'
	) );
	
    $cmb_trends->add_field( array(
		'name'    => __( 'Trend Link', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'trend_link',
		'type'    => 'text',
	) );	

	$cmb_runway = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_runway',
		'title'         => __( 'Runway Video', 'cmb2' ),
		'object_types'  => array( 'runway', ), // Post type
		 'show_names' => true, // Show field names on the left
	) );

    $cmb_runway->add_field( array(
		'name'    => __( 'Link', 'cmb2' ),
		'desc'    => __( 'Paste YouTube or Vimeo link', 'cmb2' ),
		'id'      => $prefix . 'runway_link',
		'type'    => 'oembed',
	) );
/*	
	$cmb_makeup_desc = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_makeup_desc',
		'title'         => __( 'Make-Up Description Kit', 'cmb2' ),
		'object_types'  => array( 'makeup', ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		 'show_names' => false, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		 'closed'     => false, // true to keep the metabox closed by default
	) );
	

	$cmb_makeup_desc->add_field( array(
	    'name'    => __('Make-Up Kit Description','cmb2'),
	    'desc'    => __('','cmb2'),
	    'id'      => $prefix.'makeup_description',
	    'type'    => 'wysiwyg',
	    'options' => array(),
	) );
*/
	$cmb_makeup_esp = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_makeup_esp',
		'title'         => __( 'Spanish', 'cmb2' ),
		'object_types'  => array( 'makeup', ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		 'show_names' => false, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		 'closed'     => false, // true to keep the metabox closed by default
	) );
    
    $cmb_makeup_esp->add_field( array(
		'name'    => __( 'Spanish', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'makeup_esp',
		'type'    => 'wysiwyg',
		'options' => array( 'textarea_rows' => 9, ),
	) );
	
	$cmb_makeup = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_makeup',
		'title'         => __( 'Make-Up Essentials', 'cmb2' ),
		'object_types'  => array( 'makeup', ), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		 'closed'     => false, // true to keep the metabox closed by default
	) );
	
	$makeup_group_id = $cmb_makeup->add_field( array(
        'id'          => 'makeup_repeat_group',
        'type'        => 'group',
        'description' => __( '', 'cmb' ),
        'options'     => array(
            'group_title'   => __( 'Make-Up Item {#}', 'cmb' ), // since version 1.1.4, {#} gets replaced by row number
            'add_button'    => __( 'Add Another Make-Up Item', 'cmb' ),
            'remove_button' => __( 'Remove Make-Up Item', 'cmb' ),
            'sortable'      => true, // beta
        ),
    ) );

    $cmb_makeup->add_group_field( $makeup_group_id, array(
        'name' => 'Title',
        'id'   => 'title',
        'type' => 'text',
        // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    $cmb_makeup->add_group_field( $makeup_group_id, array(
        'name' => 'Item Image',
        'id'   => 'image',
        'type' => 'file',
    ) );
    
    $cmb_makeup->add_group_field( $makeup_group_id, array(
        'name' => 'Item Description (English)',
        'id'   => 'makeup_description_en',
        'type' => 'textarea_small',
    ) );
	
    $cmb_makeup->add_group_field( $makeup_group_id, array(
        'name' => 'Item Description (Spanish)',
        'id'   => 'makeup_description_sp',
        'type' => 'textarea_small',
    ) );

    $cmb_makeup->add_group_field( $makeup_group_id, array(
        'name' => 'Link',
        'id'   => 'makeup_link',
        'type' => 'text',
    ) );
	
	$cmb_shop_style = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_shop_link',
		'title'         => __( 'Shop My Style Info', 'cmb2' ),
		'object_types'  => array( 'shop-my-style', ), // Post type
		 'show_names' => true, // Show field names on the left
	) );
    
    $cmb_shop_style->add_field( array(
		'name'    => __( 'Link', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'shop_my_style_link',
		'type'    => 'text',
	) );
	
    
    $cmb_shop_style->add_field( array(
		'name'    => __( 'Image', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'shop_my_style_image',
		'type'    => 'file',
	) );
}
