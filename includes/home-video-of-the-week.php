<?php
	/*	Video of the Week
	/**************************************************
	***************************************************/

	echo '<h2>Video of the Week</h2>';

	$args = array( 'post_type' => 'video', 'posts_per_page' => 1 );
	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post();
		the_title();

		echo'<br/>';
		$video = get_post_meta( get_the_ID(), $prefix.'video_link', true );
		echo $video.'<br/>';

	endwhile;
	wp_reset_postdata();

