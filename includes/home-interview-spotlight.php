<?php 

	/*	Interview Spotlight
	/**************************************************
	***************************************************/

	echo '<h2>Interview Spotlight</h2>';

	$args = array( 'post_type' => 'my-work', 'posts_per_page' => 1 );
	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post();
		
		the_title();
		echo '<br/>';
		the_content();

	endwhile;
	wp_reset_postdata();
