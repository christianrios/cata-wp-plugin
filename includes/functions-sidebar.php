<?php
/**
 * Register Sidebar
 */
	function cata_sidebar() {
		unregister_sidebar( 'sidebar-1' );
		register_sidebar(
			array(
				'id' => 'cata-main-sidebar',
				'name' => __( 'Main Sidebar', 'cata' ),
				'description' => __( 'Control the sidebar on the homepage.', 'cata' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget' => '</aside>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>'
			)
		);

		/* Repeat register_sidebar() code for additional sidebars. */
	}
	add_action( 'widgets_init', 'cata_sidebar' );
?>