<?php
	/*	Runway Feature
	/**************************************************
	***************************************************/

	echo '<h2>Runway Feature</h2>';

	$args = array( 'post_type' => 'runway', 'posts_per_page' => 1 );
	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post();

		$runway_link = get_post_meta(get_the_ID(), $prefix.'runway_link', true );

		echo $runway_link.'<br/>';
		the_title();
		echo '<br/>';
		the_content();
	endwhile;
	wp_reset_postdata();