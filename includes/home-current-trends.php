<?php
	/*	Current Trends
	/**************************************************
	***************************************************/

	echo '<h2>Current Trend</h2>';

	$args = array( 'post_type' => 'trend', 'posts_per_page' => 3, 'order_by'=>'ID', 'order'=>'ASC');
	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post();
		
		$trend_caption = get_post_meta(get_the_ID(), $prefix.'trend_caption', true );
		$trend_link = get_post_meta(get_the_ID(), $prefix.'trend_link', true );
		the_title();

		echo '<br/>';

		echo $trend_caption.'<br/>';
		echo $trend_link.'<br/><br/>';

	endwhile;
	wp_reset_postdata();
