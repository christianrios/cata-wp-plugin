<?php
add_action( 'admin_menu', 'cata_add_admin_menu' );
add_action( 'admin_init', 'cata_settings_init' );


function cata_add_admin_menu(  ) { 

	add_options_page( 'Twenty Fifteen Child for CataBalzano', 'Twenty Fifteen Child for CataBalzano', 'manage_options', 'twenty_fifteen_child_for_catabalzano', 'twenty_fifteen_child_for_catabalzano_options_page' );

}


function cata_settings_init(  ) { 

	register_setting( 'pluginPage', 'cata_settings' );

	add_settings_section(
		'cata_pluginPage_section', 
		__( 'Your section description', 'wordpress' ), 
		'cata_settings_section_callback', 
		'pluginPage'
	);

	add_settings_field( 
		'cata_text_field_0', 
		__( 'Settings field description', 'wordpress' ), 
		'cata_text_field_0_render', 
		'pluginPage', 
		'cata_pluginPage_section' 
	);

	add_settings_field( 
		'cata_text_field_1', 
		__( 'Settings field description', 'wordpress' ), 
		'cata_text_field_1_render', 
		'pluginPage', 
		'cata_pluginPage_section' 
	);

	add_settings_field( 
		'cata_checkbox_field_2', 
		__( 'Settings field description', 'wordpress' ), 
		'cata_checkbox_field_2_render', 
		'pluginPage', 
		'cata_pluginPage_section' 
	);


}


function cata_text_field_0_render(  ) { 

	$options = get_option( 'cata_settings' );
	?>
	<input type='text' name='cata_settings[cata_text_field_0]' value='<?php echo $options['cata_text_field_0']; ?>'>
	<?php

}


function cata_text_field_1_render(  ) { 

	$options = get_option( 'cata_settings' );
	?>
	<input type='text' name='cata_settings[cata_text_field_1]' value='<?php echo $options['cata_text_field_1']; ?>'>
	<?php

}


function cata_checkbox_field_2_render(  ) { 

	$options = get_option( 'cata_settings' );
	?>
	<input type='checkbox' name='cata_settings[cata_checkbox_field_2]' <?php checked( $options['cata_checkbox_field_2'], 1 ); ?> value='1'>
	<?php

}


function cata_settings_section_callback(  ) { 

	echo __( 'This section description', 'wordpress' );

}


function cata_options_page(  ) { 

	?>
	<form action='options.php' method='post'>
		
		<h2>Twenty Fifteen Child for CataBalzano</h2>
		
		<?php
		settings_fields( 'pluginPage' );
		do_settings_sections( 'pluginPage' );
		submit_button();
		?>
		
	</form>
	<?php

}

?>