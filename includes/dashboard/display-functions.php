<?php

function display_post_with_video($loop_args, $type, $video_id){

	$loop = new WP_Query( $loop_args );
	
	while ( $loop->have_posts() ) : $loop->the_post();
	
		$video = get_post_meta( get_the_ID(), $video_id, true );
		$embed_code = wp_oembed_get($video, array('width'=>400));

		echo $embed_code;
		echo '<hr/>';
	
		echo '<strong>';
		echo the_title();
		echo ' - ';
		echo '<a href="'.get_edit_post_link( get_the_ID(), '' ).'">Edit</a>';		
		echo ' | ';	
		echo '<a href="'.admin_url().'post-new.php?post_type='.$type.'">Add New</a>';
		echo ' | ';
		echo '<a href="'.admin_url().'edit.php?post_type='.$type.'">View All</a>';	
		echo '</strong>';
	endwhile;
	wp_reset_postdata();	
}

function display_latest_posts_dashboard($loop_args, $type){
	
	$loop = new WP_Query( $loop_args );
	
	echo '<ol>';
	
	while ( $loop->have_posts() ) : $loop->the_post();
		echo '<li>';
		the_title();
		echo ' - <a href="'.get_edit_post_link( get_the_ID(), '' ).'">Edit</a>';
		echo '</li>';
	endwhile;
	wp_reset_postdata();	
		
	echo '</ol>';
	
	echo '<strong>';
	echo '<a href="'.admin_url().'post-new.php?post_type='.$type.'">Add New</a>';
	echo ' | ';
	echo '<a href="'.admin_url().'edit.php?post_type='.$type.'">View All</a>';
	echo '</strong>';
}
