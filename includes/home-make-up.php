<?php

	/*	Make Up
	/**************************************************
	***************************************************/

	echo '<h2>Make Up</h2>';

	$args = array( 'post_type' => 'makeup', 'posts_per_page' => 1 );
	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post();
	
		the_title();
		echo '<br/>';
		the_content();

		$makeupitems = get_post_meta( get_the_ID(), 'makeup_repeat_group', true );

		foreach ( (array) $makeupitems as $key => $makeup ) {


			if ( isset( $makeup['makeup_description'] ) )
			$description = esc_html( $makeup['makeup_description'] );
			
			if ( isset( $makeup['title'] ) )
			$title = esc_html( $makeup['title'] );
		
			if ( isset( $makeup['image'] ) )
			$image = esc_html( $makeup['image'] );

			if ( isset( $makeup['makeup_link'] ) )
			$link = esc_html( $makeup['makeup_link'] );
			
			// Do something with the data
			echo '<strong>'.$title.'</strong><br/>';
			echo $image.'<br/>';
			echo $description.'<br/>';
			echo $link.'<br/>';
			echo '<br/>';
		}
	echo '<br/>';
	endwhile;
	wp_reset_postdata();
