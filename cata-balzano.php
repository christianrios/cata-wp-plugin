<?php
/*
Plugin Name: Cata's Functionality Plugin
Description: All of the important functionality of the site.
Version: 1.0
License: GPL
Author: Christian Rios
Author URI: http://www.christianrios.net/
*/

include('includes/functions-sidebar.php');

/*********************
* Custom Meta Boxes
**********************/
include('includes/custom-post-types.php');

/*********************
* Re-label "Posts" in WP Admin Menu to "Blog" (Note: Post Types are NOT changed)
**********************/
function cata_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Blog';
    echo '';
}
 
add_action( 'admin_menu', 'cata_change_post_label' );

/*********************
* Re-order left admin menu for ADMINISTRATORS
**********************/
function reorder_admin_menu( $__return_true ) {
    return array(
		'index.php', // Dashboard
		'edit.php', // Posts
		'edit.php?post_type=published', // Published Articles
		'edit.php?post_type=video', // Video of the Week
		'edit.php?post_type=my-work', // My Work
		'edit.php?post_type=trend', // Trends 
		'edit.php?post_type=runway', // Runway Features 
		'edit.php?post_type=makeup', // Make Up
		'edit.php?post_type=shop-my-style', // Shop My Style 
		'edit.php?post_type=page', // Pages 
		'separator1', // --Space--
		'upload.php', // Media
		'themes.php', // Appearance
		'edit-comments.php', // Comments 
		'users.php', // Users
		'separator2', // --Space--
		'plugins.php', // Plugins
		'tools.php', // Tools
		'options-general.php', // Settings
   );
}
add_filter( 'custom_menu_order', 'reorder_admin_menu' );
add_filter( 'menu_order', 'reorder_admin_menu' );


/*********************
* Remove Menus for Editors
**********************/
add_action( 'admin_init', 'cb_remove_submenu_pages' );
function cb_remove_submenu_pages() {

    global $user_ID;

    if ( current_user_can( 'edit_theme_options' ) && !current_user_can( 'switch_theme') ) {
	remove_menu_page('upload.php');
	remove_submenu_page('themes.php','themes.php');
    }
}

/*********************
* Adding Google Font for Admin
**********************/
function add_google_font() {
	wp_enqueue_style( 'my-google-font', '//fonts.googleapis.com/css?family=Mr+De+Haviland' );
}
add_action( 'login_enqueue_scripts', 'add_google_font' );
add_action( 'admin_enqueue_scripts', 'add_google_font' );

/*********************
* Styling Login Dashboard for WordPress
**********************/
function my_login_stylesheet() {
//    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/style-login.css' );  /*Parent Theme*/
	wp_enqueue_style( 'custom-login',get_stylesheet_directory_uri(). '/style-login.css' );	/*Child Theme*/
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );


/*********************
* Styling Admin Dashboard for WordPress
**********************/
function my_admin_stylesheet() {
//    wp_enqueue_style( 'custom-admin', get_template_directory_uri() . '/style-admin.css' );  /*Parent Theme*/
	wp_enqueue_style( 'custom-admin',get_stylesheet_directory_uri(). '/style-admin.css' );	/*Child Theme*/
}
add_action( 'admin_enqueue_scripts', 'my_admin_stylesheet' );

include('includes/functions-dashboard.php');

// unregister all widgets 
function unregister_default_widgets() {     
	unregister_widget('WP_Widget_Pages');     
	unregister_widget('WP_Widget_Calendar');     
//	unregister_widget('WP_Widget_Archives');     
//	unregister_widget('WP_Widget_Links');     
	unregister_widget('WP_Widget_Meta');     
//	unregister_widget('WP_Widget_Search');     
//	unregister_widget('WP_Widget_Text');     
//	unregister_widget('WP_Widget_Categories');     
//	unregister_widget('WP_Widget_Recent_Posts');    
//	unregister_widget('WP_Widget_Recent_Comments');     
	unregister_widget('WP_Widget_RSS');     
	unregister_widget('WP_Widget_Tag_Cloud');     
	unregister_widget('WP_Nav_Menu_Widget');     
} 
add_action('widgets_init', 'unregister_default_widgets', 11);


function cata_post_images_setup() {
    // Set default values for the upload media box
    update_option('image_default_align', 'center' );
    update_option('image_default_link_type', 'none' );
    update_option('image_default_size', 'full' );
}
add_action('after_setup_theme', 'cata_post_images_setup');
